<?php

$libInfo = Qero::getPacketInfo ('MathLib');
require 'Packets/MathLib/'. $libInfo['branchID'] .'/MathLib.php';

MathLib\includeModule ('Math');

class Cipher
{
    static function encode ($text, $key)
    {
        $len = strlen ($text);

        for ($i = 0; $i < $len; ++$i)
            $enc[] = MathLib\Math\pow (ord ($text[$i]), $key);

        return join (' ', $enc);
    }

    static function decode ($text, $key)
    {
        foreach (explode (' ', $text) as $id => $item)
            $dec .= chr (MathLib\Math\root ($item, $key));

        return $dec;
    }
}

?>
